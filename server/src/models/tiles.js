import mongoose from 'mongoose'

const Tile = mongoose.model('Tile', {
    name:{
        type: String,
        required: true,
    },
    description:{
        type: String,
        required: true,
    },
    example:{
        type: String,
        required: true,
    }
})

export default Tile

