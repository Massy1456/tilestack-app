import Tile from '../models/tiles.js'

export const getAllTiles = async (req, res) => { // get all word tiles
    const tiles = await Tile.find({})
    try{
        res.send(tiles)
    } catch (e) {
        res.status(500).send(e)
    }
}

export const getTile = async (req, res) => { // get indicated tile 
    try {
        const tile = await Tile.findById(req.params.id) // try to find indicated tile
        if(!tile){ // if not found, return 404
            res.status(404).send()
        }
        res.send(tile) // if found, return it
    } catch (e) {
        res.status(500).send(e)
    }
}

export const createTile = async (req, res) => { // create a new tile
    const newTile = new Tile(req.body)
    try {
        await newTile.save()
        res.send(newTile)
    } catch (e) {
        res.status(500).send(e)
    }
}

export const editTile = async (req, res) => {
    const edits = Object.keys(req.body) // gets the keys of the request
    
    try {
        const tile = await Tile.findById(req.params.id) // find the tile with the id value
        if(!tile){
            return res.status(404).send()
        }
        edits.forEach((edit) => { // for each edit, update it in the tile from the req.body
            tile[edit] = req.body[edit]
        })
        await tile.save()
        res.send(tile)
    } catch (e) {
        res.status(500).send(e)
    }
}

export const deleteTile = async (req, res) => {
    try {
        const tile = await Tile.findByIdAndDelete(req.params.id)
        if(!tile){
            res.status(404).send()
        }
        res.send(tile)
    } catch (e) {
        res.status(500).send(e)
    }
}

