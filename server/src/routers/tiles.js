import express from 'express'
import Tile from '../models/tiles.js'
import { getAllTiles, getTile, createTile, editTile, deleteTile } from '../controllers/tiles.js'

const router = express.Router()

router.get('/tile', getAllTiles)

router.get('/tile/:id', getTile)

router.post('/tile', createTile)

router.patch('/tile/:id', editTile)

router.delete('/tile/:id', deleteTile)

export default router