import express from 'express'
import './src/db/mongoose.js'
import cors from 'cors'
import bodyParser from 'body-parser'
import tileRouters from './src/routers/tiles.js'

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json({limit: "30mb", extended: true}))
app.use(bodyParser.urlencoded({limit: "30mb", extended: true}))
app.use(cors()) // allows us to communicate from server port to client port

app.use(tileRouters)

// create connection with database

export default app
