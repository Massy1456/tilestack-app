import app from './app.js'

const PORT = process.env.PORT || 5000

// create connection with database
app.listen(PORT, () => {
    console.log(`Server is up on Port ${PORT}`)
})


