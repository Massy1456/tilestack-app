import { createRequire } from "module";
const require = createRequire(import.meta.url);

const request = require("supertest")
const app = require('../app.js')


test('Should create a new word tile', async () => {
    await request(app).post('/tile').send({
        name: 'foo',
        description:'ooo',
        example:'bar'
    }).expect(201)
})